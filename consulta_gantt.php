<?php require_once("Connections/conexion_admin_proyectos.php"); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php
	mysql_select_db($database_conexion_proyectos, $conexion_admin_proyectos);

	$idproyecto =$_POST['idproyecto'];	

	$proyecto=
	"SELECT 
	    nombre_proyecto
	FROM
	    proyecto
	WHERE
	    idproyecto = $idproyecto";
	$q_proyecto=mysql_query($proyecto) or die(mysql_error());
	$row_proyecto=mysql_fetch_array($q_proyecto);
	$nombre_proyecto=$row_proyecto['nombre_proyecto'];

	$categorias='';
	$data='';

	$tareas =
	"SELECT 
    nombre_tarea,
    YEAR(fecha_inicio) AS anio_inicio,
    MONTH(fecha_inicio) AS mes_inicio,
    DAY(fecha_inicio) AS dia_inicio,
    YEAR(fecha_fin) AS anio_fin,
    MONTH(fecha_fin) AS mes_fin,
    DAY(fecha_fin) AS dia_fin
	FROM
    	tarea
	WHERE
    proyecto_idproyecto = $idproyecto";
	$q_tareas=mysql_query($tareas) or die(mysql_error());
	$i=0;
	while ($row_tareas=mysql_fetch_array($q_tareas)) {
		$categorias.="'".$row_tareas['nombre_tarea']."', ";
		$porcentaje=1;
		$data.='{x: Date.UTC('.$row_tareas['anio_inicio'].', '.$row_tareas['mes_inicio'].', '.$row_tareas['dia_inicio'].'),x2: Date.UTC('.$row_tareas['anio_fin'].', '.$row_tareas['mes_fin'].', '.$row_tareas['dia_fin'].'),y: '.$i.',partialFill: '.$porcentaje.'},';
       	$i++;
		
	}

?>
<div id="container"></div>
<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'xrange'
    },
    title: {
        text: 'Diagrama de Gantt'
    },
    xAxis: {
        type: 'datetime'
    },
    yAxis: {
        title: {
            text: 'Tareas'
        },
        categories: [<?php echo $categorias; ?>],
        reversed: true
    },
    series: [{
        name: '<?php echo $nombre_proyecto; ?>',
        // pointPadding: 0,
        // groupPadding: 0,
        borderColor: 'gray',
        pointWidth: 20,
        data: [<?php echo $data; ?>],
        dataLabels: {
            enabled: false
        }
    }]

});
</script>