<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 
    mysql_select_db($database_conexion_proyectos, $conexion_admin_proyectos);
?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "sis_header.php" ?>
</head>

<body style="background-color: white">

    <div id="wrapper">
        <!-- Navigation -->
        
        <?php include "sys_menu_vertical.php" ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Nuevo Recurso
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-table"></i><a href="proyectos.php"> Proyectos</a>
                            </li>
                            <li>
                                    Agregar Recurso
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- ---------------------------------------------Formulario------------------------------------------------------- -->
                <div id="resultado" class="row">  
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  
                    <div class="form-group">
                        <label>Descripcion: (*)</label>
                        <textarea class="form-control" rows="5" id="descripcion">Ingrese nombre o descripcion del recurso</textarea>
                    </div>
                    <div class="form-group">
                        <label>Cantidad: (*)</label>
                        <input type="int" class="form-control" id="cantidad" placeholder="Ingrese la cantidad del Recurso en stock" >
                    </div>  
                               
                    
                    <div id="success"></div>
                    <button id="reset" class="btn btn-default">Limpiar</button>
                    <button id="habilitar" type="sumbit" class="btn btn-default pull-right" >Aceptar</button>
                </div>
                
                </div>
          <!-- ---------------------------------------------------fin-------------------------------------------------------------- -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- JQUERY PARA COMPROBAR QUE NOMBRE, FECHA INICIO Y FECHA FIN SE INGRESEN -->

        <script type="text/javascript">
    $(document).ready(function() {
            $('#habilitar').click(function(event) {
                var descripcion = $('#descripcion').val();
                var cantidad = $('#cantidad').val();


                $.ajax({
                    type: "POST",
                    url: "recurso_alta_ok.php",
                    data: {"descripcion" : descripcion, "cantidad" : cantidad},
                    dataType: "html",
                    beforeSend: function(){
                          //imagen de carga
                          $("#resultado").html("<p align='center'><img src='images/ajax-loader.gif' /></p>");
                    },
                    error: function(){
                          alert("error petición ajax");
                    },
                    success: function(data){                                                    
                          $("#resultado").empty();
                          $("#resultado").append(data);                                     
                    }
              });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#principal').removeAttr('class');
            $('#proyecto').attr('class', 'active');    
        });
    </script>
    <script type="text/javascript">
    $('#reset').click(function(event) {
        $('#nombre').val('');
        $('#descripcion').val('');
        $('#fechainicio').val('');
        $('#fechafin').val('');
    });
    </script>

</body>

</html>