<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 
    mysql_select_db($database_conexion_proyectos, $conexion_admin_proyectos);
    $q_usuarios=mysql_query("SELECT idusuarios, nombreyapellido,estadousuario FROM usuarios");
?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "sis_header.php" ?>
</head>

<body style="background-color: white">

    <div id="wrapper">
        <!-- Navigation -->
        
        <?php include "sys_menu_vertical.php" ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Nuevo proyecto
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-table"></i><a href="proyectos.php"> Proyectos</a>
                            </li>
                            <li>
                                    Agregar proyecto
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- ---------------------------------------------Formulario------------------------------------------------------- -->
                <div id="resultado" class="row">  
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Nombre (*):</label>
                        <input type="text" class="form-control" placeholder="Ingrese nombre del proyecto" id="nombre">
                    </div> 
                    <div class="form-group">
                        <label>Descripcion:</label>
                        <textarea class="form-control" rows="5" id="descripcion"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Fecha de incio (*):</label>
                        <input type="date" class="form-control" id="fechainicio">
                    </div>  
            
                    <div class="form-group">
                        <label>Fecha de finalizacion (*):</label>
                        <input type="date" class="form-control" id="fechafin">
                    </div>
                    <div class="form-group">
                        <label>Tipo de encargado:</label>
                        <select id="tipoencargado" class="form-control">
                            <option value="0">Seleccione tipo encargado</option>
                            <option value="2">Usuario</option>
                            <option value="1">Proveedor</option>
                        </select>
                    </div>
                    <div id="selecproveedor" class="form-group" style="display:none;">
                        <label>Proveedores:</label>
                        <select id="idproveedor" class="form-control">
                            <option value="1">proveedor1</option>
                            <option value="2">proveedor2</option>
                            <option value="3">proveedor3</option>
                        </select>
                    </div>
                    <div class="row" style="display:none;" id="usuario_ocupado">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable" align="center">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-times fa-2x" aria-hidden="true"></i>
                            <br>
                            <strong>El usuario se encuentra realizando otra tarea!</strong>
                        </div>
                    </div>
                    </div>
                    <div id="selectusuario" class="form-group" ">
                        <label>Usuario:</label>
                        <select id="idusuario" class="form-control">
                        <option>Seleccione un usuario</option>
                        <?php 
                        while ($row_usuarios=mysql_fetch_array($q_usuarios)) { 
                            if ($row_usuarios['estadousuario']==2) {
                                $estado_us="(Usuario ocupado)";
                            }
                            else{
                                $estado_us="";
                            }
                            ?>
                             <option value="<?php echo $row_usuarios['idusuarios'] ?>"><?php echo $row_usuarios['nombreyapellido'].' '.$estado_us ?></option>
                         <?php } ?>
                        </select>
                    </div>
                    <div id="success"></div>
                    <button id="reset" class="btn btn-default">Limpiar</button>
                    <button id="habilitar" type="sumbit" class="btn btn-default pull-right" >Aceptar</button>
                </div>
                
                </div>
          <!-- ---------------------------------------------------fin-------------------------------------------------------------- -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- JQUERY PARA COMPROBAR QUE NOMBRE, FECHA INICIO Y FECHA FIN SE INGRESEN -->

    <script type="text/javascript">
    $(document).ready(function() {

        $('#fechainicio').change(function(event) {
            comprobar();
         });

        $('#fechafin').change(function(event) {
            comprobar();
        });
       
        $('#nombre').change(function(event) {
            comprobar();
        });

        function comprobar(){

            var habilitai=$('#fechainicio').val();
            var habilitaf=$('#fechafin').val();
            var habilitan=$('#nombre').val();
            
            if (habilitai!='' && habilitan!='' && habilitaf!='') {
            $('#habilitar').removeAttr('disabled');
            }
           else{
            $('#habilitar').attr('disabled', true);;
            };
        }
    });
    </script>

    <script type="text/javascript">
    $('#tipoencargado').change(function(event) {
        var tipoencargado = $('#tipoencargado').val();
        if (tipoencargado==1) {
            $('#selecproveedor').show('slow');
            $('#selectusuario').hide('slow');
            
        }
        else{
            if (tipoencargado==2) {
                $('#selecproveedor').hide('slow');
                $('#selectusuario').show('slow');
            }
            else{
                $('#selecproveedor').hide('slow');
                $('#selectusuario').hide('slow');
            } ;
        } ;
    });
    </script>

<!-- SCRIPT PARA VER SI EL USUARIO YA ESTA REALIZANDO OTRA TAREA -->
    <script type="text/javascript">
     $('#idusuario').change(function(event) {
         var idusuario_sel = $('#idusuario').val();
         $.ajax({
                    type: "POST",
                    url: "usuario_ocupado.php",
                    data: {"idusuario_sel" : idusuario_sel},
                    dataType: "html",
                    success: function(data){                                                    
                        if (data==2) {
                        $('#usuario_ocupado').show('slow');
                        }
                        else {
                            $('#usuario_ocupado').hide('slow');
                        };                                  
                    }
              });
     });
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
            $('#habilitar').click(function(event) {
                var nombre = $('#nombre').val();
                var descripcion = $('#descripcion').val();
                var fechainicio = $('#fechainicio').val();
                var fechafin = $('#fechafin').val();
                var tipojefe = $('#tipoencargado').val();//1 proveedor, 2 usuario
                var idproveedor =$('#idproveedor').val();
                var idusuario =$('#idusuario').val();

                $.ajax({
                    type: "POST",
                    url: "proyectos_alta_ok.php",
                    data: {"tipojefe" : tipojefe, "idproveedor" : idproveedor, "idusuario" : idusuario},
                    dataType: "html",
                    beforeSend: function(){
                          //imagen de carga
                          $("#resultado").html("<p align='center'><img src='images/ajax-loader.gif' /></p>");
                    },
                    error: function(){
                          alert("error petición ajax");
                    },
                    success: function(data){                                                    
                          $("#resultado").empty();
                          $("#resultado").append(data);                                     
                    }
              });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#principal').removeAttr('class');
            $('#proyecto').attr('class', 'active');    
        });
    </script>
    <script type="text/javascript">
    $('#reset').click(function(event) {
        $('#nombre').val('');
        $('#descripcion').val('');
        $('#fechainicio').val('');
        $('#fechafin').val('');
    });
    </script>

</body>

</html>