<?php @SESSION_START();?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "sis_header.php" ?>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        
        <?php include "sys_menu_vertical.php" ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Sistema de Gestion de Proyectos 
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa  fa-paperclip"></i> Gestiona: Proyectos, Tareas, Recursos, Usuarios y el Personal
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>UTN - FRT (Administracion de Recursos 4k3 - 2017)</strong> 
                        </div>
                    </div>
                </div>
                <!-- /.row -->
               
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
